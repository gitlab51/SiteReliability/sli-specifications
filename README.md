# Specifications

![image](SLI Specifications.jpg)


| Step | User Journey | Detail | SLI Type | SLI Specification (What is measured) | SLI Implementations(Where is it measured) | SLO(Measures) | Justification |
| --- | --- | --- | --- | --- | --- | --- | --- |
| 1 | Buy Stuff UI Page Load | /api/getSKUs | Availability | Profile Page should load successfully showing appropriate List of SKUs IDs | Load Balancer (Complexity is low therefore suitable) |  Success: HTTP(S) requests codes = 2XX | Addresses Client to Server &amp; back |
| 2 | Buy Stuff UI Page Load |   | Latency | How long profile page takes to render fully (ie retrieve and list SKUs IDs) | Load Balancer (Complexity is low therefore suitable) | 99% of home page requests in the past 28 days served in \&lt; 100ms | Addresses Client to Server &amp; back |
| 3 | SKU Details Response |   | Latency | How long profile page takes to render fully (ie retrieve and list SKUs Details) | Load Balancer (Complexity is low therefore suitable) | 99% of home page requests in the past 28 days served in \&lt; 100ms | Addresses optional client to Play Store &amp; back |
| 4 | Status Code; Order ID &amp; Purchase TokenReturned to Client |   | Availability | This would be measured on the client side perhaps with a probe, however confining, for simplicity and realistic controllability, we could confirm the status code recevied on the load ablancer and as a complement measure the response time this has taken.   | Load Balancer (Complexity is low therefore suitable)  | 99.9% of received token requests are valid (dependant on status code) and within acceptable time frame to the load balancer over a period of 28 days  | Addresses Server to Play Store &amp; back |
| 5 | Complete Purchase | /api/completePurchase | Quality | The proportion of valid requests served without degrading quality | Server - Server logs |   | Addresses Server to Play Store &amp; back |
| 6 | Complete Purchase |   | Latency |   | Server - Server logs | 99% of home page requests in the past 28 days served in \&lt; 100ms | Addresses Server to Play Store &amp; back |
| 7 | Complete Purchase | /api/completePurchase | Quality | The proportion of valid requests served without degrading quality | Application Server Metrics at &quot;update account&quot; on server | Check Status Code |   |
| 8 | Complete Purchase |   | Latency | How long profile page takes to render fully (ie retrieve and list | Load Balancer (Complexity is low therefore suitable) | 99% of home page requests in the past 28 days served in \&lt; 100ms | Addresses Client to Server &amp; back. Addresses &quot;Update Account&quot; process latency |
|   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |


**Journey Walk-through**
Based on the assumption that the clients are outside the Google Cloud Platform we can negate this. The clients would be mobile phones, tablets that would require connectivity over the internet to get through and make contact with the cloud hosted solution, usually starting load balancer entry point. For completeness and for the support team to be aware it would be good to know about the responses from the server to the client but we would not be in control to be able to fix the connectivity issues. For this purpose the entire metrics and hence SLO for items 1,2,3,8 are not essential.
